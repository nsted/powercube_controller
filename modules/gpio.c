/*
 * Copyright 2015 Globacore Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
* GPIO
* GPIO board controller. Definition of functions and pins. It is also a sparkfun board.
*/

#define LEFT_BUZZER 44
#define RIGHT_BUZZER 45

#define RED_PIN 14
#define GREEN_PIN 15
#define BLUE_PIN 49

// digitalWrite(), pinInit() & ioInit() are based on
// http://learn.sparkfun.com/tutorials/
// sparkfun-blocks-for-intel-edison---gpio-block

// 1 == ON, 0 == OFF
void digitalWrite(int pin, int value) {
  char c[100];
  sprintf(c, "echo %d > /sys/class/gpio/gpio%d/value\n", value, pin);
  system(c);
}

void pinInit(int pin) {
  char c[100];
  sprintf(c, "echo %d > /sys/class/gpio/export\n", pin);
  system(c);

  char d[100];
  sprintf(d, "echo out > /sys/class/gpio/gpio%d/direction\n", pin);
  system(d);

  digitalWrite(pin, 0);
}

void ioInit() {
  pinInit(LEFT_BUZZER);
  pinInit(RIGHT_BUZZER);
  pinInit(RED_PIN);
  pinInit(GREEN_PIN);
  pinInit(BLUE_PIN);
}

void ledController(int r, int g, int b)
{
  if(r == -1 || g == -1 || b == -1) return;
  digitalWrite(RED_PIN, r);
  digitalWrite(GREEN_PIN, g);
  digitalWrite(BLUE_PIN, b);
}

void buzzController(int l, int r)
{
  if(l == -1 || r == -1) return;
  digitalWrite(LEFT_BUZZER, l);
  digitalWrite(RIGHT_BUZZER, r);
}