
/*
 * Copyright 2015 Globacore Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Lot of the ideas and algos are originally from
 * https://github.com/sparkfun/LSM9DS0_Breakout/
 *
 */

/*
* POWERCUBE
* Main file for the powercube controller
*/

#include <getopt.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h>

#include "edison-9dof-i2c.h"
#include "modules/freedom.c"
#include "modules/gpio.c"
#include "modules/socket.c"


//--------------------------------------------------------------------------
//  MAIN
//--------------------------------------------------------------------------

int main (int argc, char **argv)
{
  ioInit(); // not necessary

  //LED and motor sequence to test once program executes
  usleep(2000000);

  digitalWrite(LEFT_BUZZER, 1);
  usleep(2000000);
  digitalWrite(LEFT_BUZZER, 0);

  digitalWrite(RIGHT_BUZZER, 1);
  usleep(2000000);
  digitalWrite(RIGHT_BUZZER, 0);

  digitalWrite(RED_PIN, 1);
  usleep(2000000);
  digitalWrite(RED_PIN, 0);

  digitalWrite(GREEN_PIN, 1);
  usleep(2000000);
  digitalWrite(GREEN_PIN, 0);

  digitalWrite(BLUE_PIN, 1);
  usleep(2000000);
  digitalWrite(BLUE_PIN, 0);

  int file;
  int16_t temp;
  uint8_t data[2] = {0};
  Triplet a_bias = {0}, g_bias = {0}, m_bias = {0};
  FTriplet m_scale;
  int opt, option_index, help = 0, option_dump = 0;
  
  char *server_ip = SOCKET_SERVER_IP;
  int server_port = SOCKET_SERVER_PORT;

  OptionMode option_mode = OPTION_MODE_ANGLES;
  float declination = 0.0;

  while ((opt = getopt_long(argc, argv, "d:hm:u",
                            long_options, &option_index )) != -1) {
    switch (opt) {
      case 'd' :
        declination = atof (optarg);
        break;
      case 'm' :
        if (strcmp (optarg, "sensor") == 0)
          option_mode = OPTION_MODE_SENSOR;
        else if (strcmp (optarg, "angles") == 0)
          option_mode = OPTION_MODE_ANGLES;
        else
          help = 1;
        break;
      case 'u' :
        option_dump = 1;
        break;
      case 's' :
        server_ip = optarg;
        break;
      case 'p' :
        server_port = atoi (optarg);
        break;
      default:
        help = 1;
        break;
    }
  }

  int sock = -1;

  if (help || argv[optind] != NULL) {
      printf ("%s [--mode <sensor|angles>] [--dump]\n", argv[0]);
      return 0;
  }

  if (!read_bias_files (&a_bias, &g_bias, &m_bias, &m_scale))
    return 1;

  file = init_device (I2C_DEV_NAME);
  if (file == 0)
    return 1;

  if (option_dump) {
    dump_config_registers(file);
    printf ("\n");
  }

  init_gyro(file, GYRO_SCALE_245DPS);
  init_mag(file, MAG_SCALE_2GS);
  init_acc(file, ACCEL_SCALE_2G);

  // temperature is a 12-bit value: cut out 4 highest bits
  read_bytes (file, XM_ADDRESS, OUT_TEMP_L_XM, &data[0], 2);
  temp = (((data[1] & 0x0f) << 8) | data[0]);
  printf ("Temperature: %d\n", temp);

  if (option_mode == OPTION_MODE_SENSOR)
    printf ("  Gyroscope (deg/s)  | Magnetometer (mGs)  |   Accelerometer (mG)\n");
  else
    printf ("      Rotations (mag + acc):\n");

  int accx_last = 0;

  int anglex_last = 0;
  
  int gyroy_max = 200;
  int gyroy_last  = 0;
  int gyroy_cnt = 0;

  int gyroz_max = 190; //200
  int gyroz_last  = 0;
  int gyroz_cnt = 0;
  int rotat_cnt = 0;

  char* message = "";

  while (1) {

  	while (sock == -1)
    {
      sock = socketConnect(server_ip, server_port);
      usleep (1000000);
    }

    accx_last = 0;
    anglex_last = 0;
    gyroy_last  = 0;
    gyroy_cnt = 0;
    gyroz_last  = 0;
    gyroz_cnt = 0;
    rotat_cnt = 0;

 	while (sock != -1) {
	    FTriplet gyro, mag, acc, angles1;

	    usleep (10000);

	    read_gyro (file, g_bias, GYRO_SCALE_245DPS, &gyro);
	    read_mag (file, m_bias, m_scale, MAG_SCALE_2GS, &mag);
	    read_acc (file, a_bias, ACCEL_SCALE_2G, &acc);

	    calculate_simple_angles (mag, acc, declination, &angles1);

	    //drop
	    // if (anglex_last == 0 && gyroz_last == 0)
	    // {
	    //   message = checkGesture(acc.x*1000+60, "drop", "", 500, 1300, &accx_last);
	    // }

	    //move down
	    if (gyroy_cnt > 0)
	      gyroy_cnt--;

	    if (accx_last == 0 && anglex_last == 0 && gyroy_cnt == 0 && gyroz_last == 0 && strcmp(message, "") == 0)
	    {
	      if (gyro.y > gyroy_max && gyroy_last == 0)
	      {
	        gyroy_last = gyro.y;
	       
	        message = "moveDown";
	      }
	      else if (gyroy_last != 0)
	      {
	        if (gyro.y < -100) {
	          message = "moveDownEnd";
	          gyroy_last = 0;
	          gyroy_cnt = 17;
	        }
	      }
	    }

	    //move left - right
	    if (gyroz_cnt > 0)
	      gyroz_cnt--;
	    
	    if (accx_last == 0 && anglex_last == 0 && gyroz_cnt == 0 && gyroy_last == 0 && strcmp(message, "") == 0)
	    {
	      if (abs(gyro.z) > gyroz_max && gyroz_last == 0)
	      {
	        gyroz_last = gyro.z;

	        if (gyroz_last < 0) {
	          message = "moveLeft";
	        } else {
	          message = "moveRight";
	        }
	      }
	      else if (gyroz_last != 0)
	      {
	          if ((gyroz_last > 0 && gyro.z < -100) ||
	              (gyroz_last < 0 && gyro.z > 100)) {

		          if (gyroz_last < 0) {
		            message = "moveLeftEnd";
		          } else {
		            message = "moveRightEnd";
		          }
		          gyroz_last = 0;
		          gyroz_cnt = 17;
	        }
	      }
	    }

	    if (rotat_cnt > 0)
	      rotat_cnt--;

	    //rotate
	    if (accx_last == 0 && gyroz_last == 0 && rotat_cnt == 0 && gyroy_last == 0 && strcmp(message, "") == 0)
	    {
	      message = checkGesture(angles1.x, "rotateLeft", "rotateRight", 10, 38, &anglex_last);
	      if (strcmp(message, "") != 0)
	      {
	        if (anglex_last == 0)
	          rotat_cnt = 30;
	      }
	    }

	    // For debug purposes:
	    // printf ("gyro: %4.0f %4.0f %4.0f | ", gyro.x, gyro.y, gyro.z);
	    // printf ("mag: %4.0f %4.0f %4.0f | ", mag.x*1000, mag.y*1000, mag.z*1000);
	    // printf ("acc: %4.0f %4.0f %5.0f\n", acc.x*1000, acc.y*1000, acc.z*1000);

	    // printf ("pitch: %4.0f, roll: %4.0f, yaw: %4.0f\n",
	    //         angles1.x, angles1.y, angles1.z);

	    if(strcmp(message, "") != 0)
      	{
      		int success = 1;
        	success *= sendSocketMsg(sock, message);
          
	        if (!success) {
	          printf("No success \n");
	          socketClose(&sock);
	          sock = -1;
	          ledController(0, 0, 0);
	          buzzController(0, 0);
        	}
	        else {
	          printf("%s\n", message);
	        }

          	message = "";
      	}

	    if(sock != -1) receiveSocketMsg(sock);
	  }

	}

  return 0;
}

