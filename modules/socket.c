/*
 * Copyright 2015 Globacore Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
* SOCKET
* Socket controller.
*/

#define SOCKET_SERVER_IP   "127.0.0.1"
#define SOCKET_SERVER_PORT 8888

int sendSocketMsg(int sock, char message[]) {
 // printf("%d sendSocketMsg \n",sock);

  char c;
  ssize_t x = recv(sock, &c, 1, MSG_PEEK);

  if (x == 0) {
    printf("handle FIN from A \n");
    return 0;
  } 

  send(sock , message , strlen(message) , 0);
  return 1;
}

int socketConnect(const char* server_ip, int server_port) {
  struct sockaddr_in server;

  printf("Connecting to %s:%i\n", server_ip, server_port);

  //Create socket
  int sock = socket(AF_INET , SOCK_STREAM , 0);
  if (sock == -1)
  {
    printf("Could not create socket\n");
    return -1;
  }

  struct timeval tv;
  tv.tv_sec = 0;  /* 30 Secs Timeout */
  tv.tv_usec = 1000;  // Not init'ing this can cause strange errors

  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
  int on = 1;
  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,(const char *) &on, sizeof(on));

  printf("Socket created\n");
     
  server.sin_addr.s_addr = inet_addr( server_ip );
  server.sin_family = AF_INET;
  server.sin_port = htons( server_port );
 
  //Connect to remote server
  if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
  {
    perror("Connect failed. Error\n");
    // return 0;
    return -1;
  }
     
  printf("Connected\n");
  char* message = "What up!";
  sendSocketMsg(sock, message);

  return sock;
}

void socketClose(int *sock) {
  close(*sock);
  *sock = -1;
}

void receiveSocketMsg(int sock) {
  //printf("receiveSocketMsg\n");
  int count, size_recv, total_size = 0;
  int CHUNK_SIZE = 512;
  char chunk[CHUNK_SIZE];
  char * pch;
  int command, r, g, b, l, d;

  memset(chunk ,0 , CHUNK_SIZE);  //clear the variable
  if((size_recv =  recv(sock , chunk , CHUNK_SIZE , 0) ) > 0)
  {
    count = 0;
    command = -1;
    r = -1;
    g = -1;
    b = -1;
    l = -1;
    d = -1;
    total_size += size_recv;
    printf("chunk: %s\n" , chunk);

    sendSocketMsg(sock, chunk);
    
    pch = strtok (chunk,":");

    while (pch != NULL)
    {
      if(count == 0) // first item of the split
      {
        if(strcmp(pch, "1") == 0) // 1 - LED
          command = 1;
        else if(strcmp(pch, "2") == 0) // 2 - Buzzer
          command = 2;
      }
      else
      {
        if(command == 1)
        {
          if(count == 1) r = atoi(pch);
          else if(count == 2) g = atoi(pch);
          else if(count == 3) b = atoi(pch);
        }
        if(command == 2)
        {
          if(count == 1) l = atoi(pch);
          else if(count == 2) d = atoi(pch);
        }
      }

      count++;
      pch = strtok (NULL, ",");
    }

    if(command == 1)
    {
      ledController(r, g, b);
    }
    else if(command == 2)
    {
      buzzController(l, d);
    }


  }
}

//--------------------------------------------------------------------------
//  GESTURE
//--------------------------------------------------------------------------

char* checkGesture(int value, char* event1, char* event2, int value_min, int value_max, int *value_last)
{
//printf("%i, %s, %s, %i, %i, %i\n", value, event1, event2, value_max, *value_last);

char* message = "";
if (abs(value) > value_max && *value_last == 0)
{
  *value_last = value;

  if (value > 0) {
    message = event1;
  } else {
    message = event2;
  }
}
else if (*value_last != 0)
{
  if ((*value_last > 0 && value < value_min) ||
      (*value_last < 0 && value > -value_min)) {

    char* temp;
    if (*value_last > 0) {
      temp = event1;
    } else {
      temp = event2;
    }
    *value_last = 0;

    if (strcmp(temp, "") != 0) {
      message = malloc(strlen(temp) + 4);
      strcpy(message, temp);
      strcat(message, "End");
    }
  }
}
return message;
}

